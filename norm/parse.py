"""
parse Python source code into class definitions
"""

import ast

from dataclasses import dataclass


@dataclass
class Annotation:
    """
    eg: Annotation('some_field', 'str')
    """

    name: str
    _type: str  # keep this a string for now. we'll try to resolve types later in the pipeline
    nullable: bool = False


def get_field_annotation(annotation: ast.AnnAssign) -> Annotation:
    """
    only annotations matching the following pattern are allowed:
    - name: A
    - name: A | None
    - name: None | A
    """
    if not (
        isinstance(annotation.target, ast.Name)
        and (
            isinstance(annotation.annotation, ast.Name)
            or isinstance(annotation.annotation, ast.BinOp)
        )
    ):
        print(
            f"AnnAssign target: {annotation.target}; annotation: {annotation.annotation}"
        )
        raise ValueError(f"annotation field name and type nodes must be ast.Name")

    nullable = False
    if isinstance(annotation.annotation, ast.BinOp):
        type_name = resolve_binop_type(annotation.annotation)
        nullable = True
    else:
        type_name = annotation.annotation.id

    return Annotation(annotation.target.id, type_name, nullable)


def resolve_binop_type(binop: ast.BinOp) -> str:
    """
    the rules right now are pretty strict: only union types of the form:
    A | None
    None | A
    are allowed

    TODO:
    - check out if we can somehow infer json-ness
    - how to support types like UUID or datetime
      - for datetime, eventually check for timezones and create tz-aware fields if needed
    """
    if (
        isinstance(binop.left, ast.Name)
        and isinstance(binop.right, ast.Constant)
        and binop.right.value is None
    ):
        return binop.left.id
    elif (
        isinstance(binop.left, ast.Constant)
        and binop.left.value is None
        and isinstance(binop.right, ast.Name)
    ):
        return binop.right.id
    else:
        raise ValueError("union type must be of form A | None or None | A")


def is_elligible_class(clazz: ast.ClassDef) -> bool:
    """
    validate that we have enough information in the class definition
    to generate sql

    right now, this means we check for:
    - a non-empty list of AnnAssign nodes within the class body
    - an absense of an __init__ method
    """
    has_annotations = False
    for node in clazz.body:
        match node:
            case ast.FunctionDef(name="__init__"):
                return False
            case ast.AnnAssign(_):
                has_annotations = True
            case _:
                continue
    return has_annotations


def get_class_annotations(mod: ast.Module) -> dict[str, list[Annotation]]:
    classes = [i for i in mod.body if isinstance(i, ast.ClassDef)]
    annotations = dict()
    for _class in classes:
        if not is_elligible_class(_class):
            continue

        field_annotations = [
            get_field_annotation(i) for i in _class.body if isinstance(i, ast.AnnAssign)
        ]
        annotations[_class.name] = field_annotations
    return annotations
