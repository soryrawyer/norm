"""
functions for generating SQL ddl statements
"""

from norm.parse import Annotation


def to_ddl(annotation: Annotation):
    """
    convert an annotation into a field declaration for a postgres
    create table statement
    """
    sql_type = {
        "int": "int",
        "str": "text",
        "bool": "boolean",
    }[annotation._type]
    nullable = "not null" if not annotation.nullable else ""
    return f"{annotation.name} {sql_type} {nullable}"


def annotations_to_sql(class_name: str, annotations: list[Annotation]) -> str:
    table = f"create table {class_name.lower()} (\n"
    table += ",\n".join(f"    {to_ddl(i)}" for i in annotations)
    table += ");"
    return table
