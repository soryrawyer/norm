"""
generate sql DDL statements from Python code

python -m norm ddl generate ?
"""

import argparse
import ast
import sys

from norm.ddl import annotations_to_sql
from norm.parse import get_class_annotations


def _parse_args():
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("filename")
    return parser.parse_args()


def main():
    args = _parse_args()
    print(f"parsing {args.filename}", file=sys.stderr)
    source = open(args.filename).read()
    a = ast.parse(source)
    annotations = get_class_annotations(a)
    tables = list(annotations_to_sql(k, v) for (k, v) in annotations.items())
    return "\n\n".join(tables)


if __name__ == "__main__":
    print(main())
