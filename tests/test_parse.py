"""
unit tests for the parse module

in general, each test scenario has the following fixtures:
- a string containing some Python code
- an ast.Module object, the result of parsing the code string
- an ast.ClassDef object
- class annotations (if applicable)
"""

import ast
import pytest


from norm.parse import (
    get_class_annotations,
    get_field_annotation,
    Annotation,
    is_elligible_class,
)


def get_class_def(mod: ast.Module) -> ast.ClassDef:
    classes = list(i for i in mod.body if isinstance(i, ast.ClassDef))
    assert len(classes) == 1
    return classes[0]


## in-scope class definitions


@pytest.fixture
def class_source():
    return """
@dataclass
class Test:
    x: int
    y: str
    """


@pytest.fixture
def module_ast(class_source) -> ast.Module:
    return ast.parse(class_source)


@pytest.fixture
def class_ast(module_ast: ast.Module) -> ast.ClassDef:
    return get_class_def(module_ast)


@pytest.fixture
def create_table_sql():
    return """
create table test (
    x int not null,
    y text not null);
    """


@pytest.fixture
def nullable_source():
    return """
@dataclass
class TestTwo:
    x: int | None
"""


@pytest.fixture
def nullable_mod(nullable_source) -> ast.Module:
    return ast.parse(nullable_source)


@pytest.fixture
def nullable_class(nullable_mod: ast.Module) -> ast.ClassDef:
    return get_class_def(nullable_mod)


@pytest.fixture
def create_table_sql_nullable():
    return """
create table testtwo (
    x int );
    """


@pytest.fixture
def class_annotations(create_table_sql, create_table_sql_nullable):
    return {
        "Test": (
            [Annotation("x", "int"), Annotation("y", "str")],
            create_table_sql,
        ),
        "TestTwo": (
            [Annotation("x", "int", True)],
            create_table_sql_nullable,
        ),
    }


@pytest.fixture
def create_table_class_lookup(create_table_sql, create_table_sql_nullable):
    return {
        "Test": create_table_sql,
        "TestTwo": create_table_sql_nullable,
    }


## out of scope class definitions


@pytest.fixture
def non_dc_source():
    return """
class NonDataclass:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    """


@pytest.fixture
def non_dc_mod(non_dc_source):
    return ast.parse(non_dc_source)


@pytest.fixture
def non_dc_class(non_dc_mod: ast.Module) -> ast.ClassDef:
    return get_class_def(non_dc_mod)


@pytest.fixture
def empty_class_source():
    return """
class empty:
    pass
    """


@pytest.fixture
def empty_class_mod(empty_class_source):
    return ast.parse(empty_class_source)


@pytest.fixture
def empty_class(empty_class_mod) -> ast.ClassDef:
    return get_class_def(empty_class_mod)


def test_get_field_annotations(
    class_ast: ast.ClassDef,
    nullable_class: ast.ClassDef,
):
    annotations = list(
        get_field_annotation(i) for i in class_ast.body if isinstance(i, ast.AnnAssign)
    )
    assert len(annotations) == 2
    x_int, y_str = annotations
    assert x_int.name == "x"
    assert x_int._type == "int"
    assert not y_str.nullable
    assert y_str.name == "y"
    assert y_str._type == "str"
    assert not y_str.nullable

    annotations = list(
        get_field_annotation(i)
        for i in nullable_class.body
        if isinstance(i, ast.AnnAssign)
    )

    x_int_nullable = annotations[0]
    assert x_int_nullable.nullable


def test_get_class_annotations(
    module_ast: ast.Module,
    nullable_mod: ast.Module,
    class_annotations,
    non_dc_mod: ast.Module,
    empty_class_mod: ast.Module,
):
    got = get_class_annotations(module_ast)
    assert "Test" in got
    assert got["Test"] == class_annotations["Test"][0]

    got = get_class_annotations(nullable_mod)
    assert "TestTwo" in got
    assert got["TestTwo"] == class_annotations["TestTwo"][0]

    got = get_class_annotations(non_dc_mod)
    assert got == dict()

    got = get_class_annotations(empty_class_mod)
    assert got == dict()


def test_is_elligible_class(class_ast, non_dc_class, empty_class):
    assert is_elligible_class(class_ast)
    assert not is_elligible_class(non_dc_class)
    assert not is_elligible_class(empty_class)
