"""
unit tests for the ddl module
"""

from typing import Tuple
import pytest

from norm.parse import Annotation
from norm.ddl import annotations_to_sql, to_ddl


@pytest.fixture
def create_table_sql():
    return """
create table test (
    x int not null,
    y text not null);
    """


@pytest.fixture
def field_annotations() -> list[Tuple[Annotation, str]]:
    return [
        (Annotation("x", "int"), "x int not null"),
        (Annotation("y", "str", nullable=True), "y text"),
    ]


@pytest.fixture
def create_table_sql_nullable():
    return """
create table testtwo (
    x int );
    """


@pytest.fixture
def class_annotations(create_table_sql, create_table_sql_nullable):
    return {
        "Test": (
            [Annotation("x", "int"), Annotation("y", "str")],
            create_table_sql,
        ),
        "TestTwo": (
            [Annotation("x", "int", True)],
            create_table_sql_nullable,
        ),
    }


def test_to_ddl(field_annotations: list[Tuple[Annotation, str]]):
    for annotation, ddl in field_annotations:
        assert to_ddl(annotation).strip() == ddl


def test_get_create_table_sql(
    class_annotations: dict[str, Tuple[list[Annotation], str]]
):
    for name, (annotations, ct) in class_annotations.items():
        assert annotations_to_sql(name, annotations) == ct.strip()
