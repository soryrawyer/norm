# norm

norm - not an orm

norm is a project to convert Python code to SQL statements. It was inspired by projects like Prisma's [Schema](https://www.prisma.io/docs/concepts/components/prisma-schema) and [CLI](https://www.prisma.io/docs/concepts/components/prisma-cli), and is an attempt to imagine what a smaller alternative to something like [SQLAlchemy](https://www.sqlalchemy.org/) would look like.

## Installing and running

`norm` uses Pipenv for dependency management, and the project can be run as a module like so: `python -m norm examples/models.py`

## License

`norm` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
