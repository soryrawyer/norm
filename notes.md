# notes

consideration for support:

- conversion between Python types and [Postgres data types](https://www.postgresql.org/docs/current/datatype.html)
  - aside from primitive types, stuff like datetime -> timestamp (with and without timezone), json, arrays
- constraints
- default values
- indexes
- enums
- configuration via class decorator and `Annotated` types
  - e.g. - `@norm(skip_init=True)` to ignore the presence of an `__init__` method or `@norm(alias="some_other_name")` to provide an alias for the DB object name
    - could be an interesting exercise to verify that a no-op decorator gets optimized away in different Python implementations
  - `Annotated` types for things like field aliases or constaints
- cli: state management
  - schema updates/migrations
  - installing/enabling packages (like [`uuid-ossp`](https://www.postgresql.org/docs/current/uuid-ossp.html))

open questions:

- when does this stop being "not an orm"? which combinations of features above would push this into orm territory?
- if this does, in fact, turn into an orm, is there a way to make the name "norm" work with that, given that "norm" is a portmanteau of "not" and "orm"?
- are the intended use cases of "infrastructure as code" and "developer ergonomics" ever at odds here?
