create table trashholiday (
    label text not null,
    start_date int not null);

create table partialfield (
    required boolean not null,
    maybe text );
