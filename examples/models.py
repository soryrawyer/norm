"""
example models to turn into sql table definitions
"""

from dataclasses import dataclass


@dataclass
class TrashHoliday:
    label: str
    start_date: int


@dataclass
class PartialField:
    required: bool
    maybe: str | None


class NonDataclass:
    def __init__(self, x, y):
        self.x = x
        self.y = y
